# Konfiguracja Goxy

Goxy jest pełnoprawnym zamiennikiem dotychczasowego proxy na twoim serwerze. Działa w modelu **SaaS** - Software as a Service - co oznacza, że nie musisz martwić się o instalowanie czegokolwiek na własnej maszynie, a konfiguracja przebiega błyskawicznie w naszym panelu.\
W tej części dowiesz się jak poprawnie połączyć swój serwer Minecraft z Goxy. Poradnik składa się z następujących etapów:

1. [Tworzenie serwera Goxy](configuration.md#tworzenie-serwera-goxy)
2. [Generowanie klucza dostępu](configuration.md#generowanie-klucza-dostepu)
3. [Konfiguracja serwera Minecraft](configuration.md#konfiguracja-serwera-minecraft)
4. [Serwer pierwszego wyboru](configuration.md#serwer-pierwszego-wyboru)

***

## Tworzenie serwera Goxy

Cała konfiguracja przebiega w Panelu Goxy.

![zrzut ekranu pokazujący widok z panelu goxy](https://i.imgur.com/2CJjxiu.png)

W celu utworzenia nowego serwera:

1. Udaj się do Panelu Goxy.
2. Przejdź do zakładki **Sieci** z menu znajdującego się po lewej stronie.\
   ![zrzut ekranu pokazujący gdzie należy kliknąć żeby przejść do wyżej wspomnianej podstrony](https://i.imgur.com/hO1hpT8.png)
3. Wybierz interesującą cie sieć.
4. Przejdź do zakładki **Serwery** z menu wybranej sieci.\
   ![zrzut ekranu pokazujący gdzie należy kliknąć żeby przejść do wyżej wspomnianej podstrony](https://i.imgur.com/KYkbsZ0.png)
5. Utwórz nowy folder.\
   ![zrzut ekranu pokazujący przycisk do utworzenia nowego folderu](https://i.imgur.com/41AGWI9.png)\
   Folder jest zwykłym kontenerem na serwery. W celu uniknięcia komplikacji, przy niektórych funkcjach Goxy lepiej jest tworzyć osobny folder dla każdego trybu na twoim serwerze.\
   Nazwa folderu może być dowolna, lecz dla wygody lepiej nadać taką, jak nazywać się będzie serwer.
6. Utwórz nowy serwer.\
   ![zrzut ekranu pokazujący przycisk do utworzenia nowego serwera](https://i.imgur.com/Hha9CWD.png)\
   Wypełnij wszystkie dane poprawnie. Nie zapomnij przypisać serwera do wcześniej utworzonego folderu!\
   ![zrzut ekranu pokazujący formularz z przykładowymi danymi do uzupełnienia](https://i.imgur.com/hpaqvsO.png)
7. Sprawdź czy wykonałeś wszystkie kroki poprawnie, struktura serwerów powinna wyglądać tak:\
   ![zrzut ekranu pokazujący nowo utworzony serwer z podanymi wyżej przykładowymi danymi](https://i.imgur.com/EC4QnEP.png)

***

## Generowanie klucza dostępu

Klucz dostępu, można powiedzieć, że jest w pewnym sensie hasłem do serwera.\
Każdy serwer podłączony do Goxy musi mieć własny, wygenerowany klucz. Bez tego późniejsze wejście na serwer nie będzie możliwe.

W celu wygenerowania nowego klucza dostępu:

1. Przejdź do ustawień swojej sieci.\
   ![zrzut ekranu pokazujący gdzie należy kliknąć żeby przejść do wyżej wspomnianej podstrony](https://i.imgur.com/nQXnbph.png)
2. W drugiej kolumnie znajdziesz klucze dostępu.\
   ![zrzut ekranu pokazujący widok z kluczami dostępu](https://i.imgur.com/kkRqSGf.png)
3. Utwórz nowy klucz dostępu.\
   ![zrzut ekranu pokazujący przycisk do utworzenia nowego klucza](https://i.imgur.com/8sgUbHr.png)\
   Nadaj mu nazwę i przypisz go do odpowiedniego folderu oraz serwera.\
   ![zrzut ekranu pokazujący formularz z przykładowymi danymi do uzupełnienia](https://i.imgur.com/7ufwMK5.png)\
   Zapisz go w bezpiecznym miejscu, bo w przyszłości nie będzie możliwości zobaczenia go! ![zrzut ekranu pokazujący informację o utworzeniu nowego klucza](https://i.imgur.com/okVMVZG.png)
4. Sprawdź czy wykonałeś wszystkie kroki poprawnie, powinieneś widzieć na liście świeżo wygenerowany klucz dostępu:\
   ![zrzut ekranu pokazujący nowo utworzony klucz z podanymi wyżej przykładowymi danymi](https://i.imgur.com/8VGAmC7.png)

***

## Konfiguracja serwera Minecraft

Goxy wspiera serwery Minecraft oparte o silnik Spigot od wersji 1.8.9, aż do najnowszej.

W celu konfiguracji serwera Minecraft:

1. Wyłącz serwer, który będziesz konfigurował.
2. Pobierz oficjalny plugin Goxy z naszego panelu - przejdź do wybranej sieci, następnie do zakładki `Serwery` i kliknij przycisk `POBIERZ PLUGIN`.\
   ![zrzut ekranu z panelu pokazujący wyżej wspomniany przycisk](images/plugin/download-view.png)
3. Przenieś pobrany plugin do folderu **plugins**, w głównym katalogu twojego serwera.
4. Uruchom serwer, aby wygenerowała się konfiguracja pluginu.
5. Wyłącz ponownie serwer, aby uniknąć nadpisania danych.
6.  Otwórz plik **config.yml** z folderu **plugins/goxy**.\
    Zawartość pliku powinna wyglądać w następujący sposób:

    ```yaml
    api:
       token: <tutaj wklej klucz>
    ```
7. W polu **token** w sekcji **api** wpisz wygenerowany wcześniej klucz dostępu serwera.
8. Uruchom ponownie serwer Minecraft.

***

## Serwer pierwszego wyboru

Ostatnim krokiem jest wybraniem folderu, na który Goxy będzie kierować graczy po wejściu.\
Zrobisz to w **Liście serwerów** na samym końcu:

![zrzut ekranu pokazujący kategorię z serwerem pierwszego wyboru](https://i.imgur.com/X6oCIVm.png)

Rozwiń listę i wybierz interesujący cię folder.

***

## To koniec!

Konfiguracja Goxy jest banalnie prosta i zajmuje kilka minut.\
Jeżeli zrobiłeś wszystko poprawnie, powinieneś móc dołączyć na serwer korzystając z jednej ze zweryfikowanych domen twojej sieci.\
![zrzut ekranu pokazujący kategorię z domyslną domeną, która służy do łączenia z serwerem](https://i.imgur.com/wnUwrLE.png)

***

## Co dalej?

Goxy jest rozbudowanym narzędziem dla administratorów serwerów Minecraft.\
Wiele autorskich rozwiązań oraz opcji dostosowania proxy do własnych potrzeb. To wszystko dla Ciebie.

Następne kroki:

1. [Dodawanie nowej domeny](configuration.md)
2. [Podstawowe ustawienia Goxy](configuration.md)
3. [Najczęściej powtarzające się problemy](configuration.md)
4. [Najczęściej zadawane pytania](configuration.md)

**Masz pytanie, którego nie obejmuje dokumentacja?**\
Dołącz na nasz oficjalny serwer [Discord](https://discord.gg/3d8vU2Dsrb)!

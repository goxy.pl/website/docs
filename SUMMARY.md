# Table of contents

* [O Goxy](README.md)
* [Konfiguracja Goxy](configuration.md)
* [Konfiguracja Domeny](domain-setup.md)
* [Plugin Goxy](plugin.md)
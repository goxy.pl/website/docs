# Plugin Goxy

Ta część dokumentacji przeznaczona jest pluginowi Goxy.

## Komendy

wkrótce

## Placeholdery

Wtyczka Goxy integruje się z pluginem [PlaceholderAPI](https://www.spigotmc.org/resources/placeholderapi.6245/).

### Dostępne placeholdery

- ``%goxy_network_online%`` - liczba graczy na całej "sieci"
- ``%goxy_container_<nazwa lub id folderu>_online%`` - liczba graczy w konkretnym folderze
- ``%goxy_server_<nazwa lub id serwera>_online%`` - liczba graczy na konkretnym serwerze

### Wskazówki

- Rozszerzenie [math](https://api.extendedclip.com/expansions/math/) do PlaceholderAPI pozwala nam na zsumowanie
  liczby graczy z dwóch lub więcej serwerów. \
  Przykładowe użycie: ``%math_0:0_{goxy_server_SERWER1_online}+{goxy_server_SERWER2_online}%``

![Przykładowe użycie rozszerzenia math](images/plugin/placeholder-tip-math.png)

## Przenoszenie graczy między serwerami

Wtyczka Goxy umożliwia skonfigurowanie jej z innymi pluginami, które odpowiadają za przenoszenie graczy.
Poniżej znajdują się przykładowe użycia dla publicznie dostępnych i darmowych pluginów.

### Za pomocą komendy

Jako administrator możesz przenosić wybranego gracza na inne serwery. Możesz to zrobić, będąc tylko
na serwerze, na którym znajduje się gracz. \
Przed wykonaniem komendy należy:

- zamienić **\<gracz>** na nazwę gracza,
- zamienić **<nazwa lub id serwera/folderu>** na nazwę lub id serwera/folderu skonfigurowanego w **Liście serwerów**.

Komenda do wykonania: `/send <gracz> <nazwa lub id serwera/folderu>`

Przykładowe użycie: `/send tirex survival`

### Za pomocą NPC

#### Citizens

Przed wykonaniem komendy należy:

- zaznaczyć odpowiedniego NPC - można to zrobić podchodząc do NPC i wpisując `/npc select`,
- zamienić **<nazwa lub id serwera/folderu>** na nazwę lub id serwera/folderu skonfigurowanego w **Liście serwerów**.

Komenda do wykonania: `/npc command add send <p> <nazwa lub id serwera/folderu>` \
Przykładowe użycie: `/npc command add send <p> survival`

Więcej informacji znajdziesz w [dokumentacji Citizens](https://wiki.citizensnpcs.co/Citizens_Wiki).

#### CitizensCMD

Przed wykonaniem komendy należy:

- zaznaczyć odpowiedniego NPC - można to zrobić podchodząc do NPC i wpisując `/npc select`,
- zamienić **<nazwa lub id serwera/folderu>** na nazwę lub id serwera/folderu skonfigurowanego w **Liście serwerów**.

Komenda do wykonania: `/npccmd add console send %p% <nazwa lub id serwera/folderu>` \
Przykładowe użycie: `/npccmd add console send %p% survival`

Więcej informacji znajdziesz w [dokumentacji CitizensCMD](https://github.com/HexedHero/CitizensCMD/wiki).

#### ZNPCS

Przed wykonaniem komendy należy:

- zamienić **\<npc id>** na id stworzonego wcześniej NPC - można wyświetlić listę NPC wpisując `/znpcs list`,
- zamienić **<nazwa lub id serwera/folderu>** na nazwę lub id serwera/folderu skonfigurowanego w **Liście serwerów**.

Komenda do wykonania: `/znpcs action <npc id> add CONSOLE send %player_name% <nazwa lub id serwera/folderu>` \
Przykładowe użycie: `/znpcs action 1 add CONSOLE send %player_name% survival`

Więcej informacji znajdziesz w [dokumentacji ZNPCS](https://github.com/gonalez/znpcs/wiki).

### Za pomocą kompasu

#### DeluxeHub

Przed wpisanie akcji do konfiguracji pluginu należy:

- zamienić **<nazwa lub id serwera/folderu>** na nazwę lub id serwera/folderu skonfigurowanego w **Liście serwerów**.

Akcja do konfiguracji: `[CONSOLE] send %player% <nazwa lub id serwera/folderu>` \
Przykładowe użycie: `[CONSOLE] send %player% survival`

Więcej informacji znajdziesz w [dokumentacji DeluxeHub](https://wiki.lewisdev.fun/free-resources/deluxehub).

### Inne możliwości

#### API BungeeCord (niezalecane)

Goxy wspiera przenoszenie graczy przy użyciu kanału do komunikacji BungeeCord. Domyślnie w BungeeCord wystarczy podać
nazwę serwera. W Goxy, zamiast nazwy należy podać id serwera.

Więcej informacji znajdziesz w [dokumentacji pisania pluginów pod BungeeCord](https://www.spigotmc.org/wiki/bukkit-bungee-plugin-messaging-channel/#connect)

#### API Goxy

Plugin Goxy udostępnia API, które umożliwia przenoszenie graczy między serwerami.
Plugin zwraca informację czy udało się przenieść gracza na serwer.

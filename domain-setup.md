# Podłączanie własnej domeny do Goxy

W tej części dokumentacji dowiesz się jak prawidłowo skonfigurować domenę, aby podpiąć ją do własnej sieci w panelu oraz żeby dało się przez nią wchodzić na serwer.
W zależności od dostawcy, u jakiego posiadasz swoją domenę, mogą występować małe różnice przy konfiguracji domeny. Dlatego też ten poradnik zawiera instrukcje jak skonfigurować domenę w panelach u dostawców, którzy są najczęściej wykorzystywani.

Spis treści:
1. [Weryfikacja właśności domeny](#weryfikacja-własności-domeny)
2. [Cloudflare](#cloudflare)
3. [OVH](#ovh)
4. [Mojego dostawcy domeny nie ma w tym poradniku](#mojego-dostawcy-domeny-nie-ma-w-tym-poradniku)

---

## Weryfikacja własności domeny
Jest to pierwszy krok, pozwala on określić czy administrator sieci jest właścicielem domeny, którą próbuje dodać.

Aby zweryfikować domenę:
1. Zaloguj się do panelu Goxy
2. Wybierz zakładkę **Sieci** z menu po lewej stronie oraz wybierz twoją sieć
3. Przejdź do zakładki **Ustawienia Sieci** znajdującej się w menu po lewej stronie
4. W kategorii **Zweryfikowane domeny** kliknij zielony przycisk znajdujący się po prawej stronie

    ![zrzut ekranu pokazujący kategorię ze zweryfikowanymi domenami](images/domain-setup/weryfikacja-1.png)
5. W oknie, które wyskoczy, wpisz swoją domenę, a następnie kliknij przycisk **Dalej**

    ![zrzut ekranu pokazujący okno z możliwością uzupełnienia domeny](images/domain-setup/weryfikacja-2.png)
6. W kolejnym oknie wyświetli się ciąg znaków, który należy wpisać w rekordzie TXT.

    Teraz w poradniku znajdź swojego dostawcę domeny i udaj się do sekcji **Rekord TXT**, znajdziesz tam instrukcje odnośnie dodawania rekordu.
7. Jeżeli dodałeś już rekord, odczekaj do 10 minut, aby serwery nazw mogły się odświeżyć i kliknij przycisk **Zweryfikuj**

    ![zrzut ekranu pokazujące okno z przyciskiem do weryfikowania domeny](images/domain-setup/weryfikacja-3.png)
8. Jeżeli wszystko poszło dobrze, powinieneś zobaczyć poniższy komunikat, domena została pomyślnie zweryfikowana. 

   ![zrzut ekranu pokazujący komunikat pomyślnej weryfikacji domeny](images/domain-setup/weryfikacja-4.png)

    Możesz teraz przejść do sekcji **Rekord SRV** w części ze swoim dostawcą, aby przekierować domenę na serwer.

Jeżeli przy weryfikacji po dodaniu rekordu TXT wyskakuje błąd, możliwe, że serwery nazw jeszcze się nie odświeżyły i musisz poczekać dłużej. Może potrwać to do aż 24 godzin, lecz zwykle po maksymalnie godzinie powinno działać.

---

## Cloudflare
Ten podpunkt zakłada, że skonfigurowałeś swoją domenę w serwisie Cloudflare. Dokumentacja nie obejmuje instrukcji podłączania i konfiguracji domeny u tego dostawcy. Podstawowy poradnik możesz znaleźć [tutaj](https://developers.cloudflare.com/fundamentals/get-started/setup/add-site/).

### Rekord TXT (weryfikacja domeny)
1. Wybierz swoją domenę w panelu Cloudflare
2. Przejdź do zakładki **DNS** znajdującej się w menu po lewej stronie
3. W kategorii **DNS management** kliknij niebieski przycisk **Add record** znajdujący się po prawej stronie

    ![zrzut ekranu pokazujący miejsce, w którym znajduje się wyżej wspomniany przycisk](images/domain-setup/cloudflare-1.png)
4. Kategoria rozszerzy się o dodatkowe pola
   - w **Type** wybierz TXT
   - w **Name** wpisz _goxy
   - w **Content** wpisz ciąg znaków, który wyświetlił się w panelu Goxy podczas weryfikowania domeny

   ![zrzut ekranu pokazujący przykładowe dane jakie należy wpisać w formularzu](images/domain-setup/cloudflare-2.png)

    Jeżeli wprowadziłeś potrzebne dane, możesz kliknąć przycisk **Save**, a następnie odczekać do 10 minut, aby serwery nazw się odświeżyły. Po tym czasie wróć do panelu Goxy w celu ukończenia weryfikacji.

### Rekord SRV (kierowanie domeny na serwer)
1. Wybierz swoją domenę w panelu Cloudflare
2. Przejdź do zakładki **DNS** znajdującej się w menu po lewej stronie
3. W kategorii **DNS management** kliknij niebieski przycisk **Add record** znajdujący się po prawej stronie

   ![zrzut ekranu pokazujący miejsce, w którym znajduje się wyżej wspomniany przycisk](images/domain-setup/cloudflare-1.png)
4. Kategoria rozszerzy się o dodatkowe pola
   - w **Type** wybierz SRV
   - w **Name** wpisz _minecraft._tcp
   - w **Priority** ustaw 1
   - w **Weight** ustaw 1
   - w **Port** wpisz 25565
   - w **Target** wpisz subdomenę swojej sieci z końcówką gxy.pl - znajdziesz ją w kategorii **Zweryfikowane domeny** w ustawieniach w panelu Goxy

   ![zrzut ekranu pokazujący przykładowe dane jakie należy wpisać w formularzu](images/domain-setup/cloudflare-3.png)
   
    Jeżeli wprowadziłeś potrzebne dane, możesz kliknąć przycisk **Save**, a następnie odczekać do 10 minut, aby serwery nazw się odświeżyły. Po tym czasie możesz spróbować wejść na serwer w grze przez swoją domenę.

---

## OVH
Ten podpunkt zakłada, że masz kupioną domenę w serwisie OVH.

### Rekord TXT (weryfikacja domeny)
1. Przejdź do swojej domeny w panelu OVH
2. Przejdź do zakładki **Strefa DNS** znajdującej się w menu nad informacjami o domenie
3. Kliknij przycisk **Dodaj rekord** znajdujący się po prawej stronie

   ![zrzut ekranu pokazujący miejsce, w którym znajduje się wyżej wspomniany przycisk](images/domain-setup/ovh-1.png)
4. Na stronie wyświetli się formularz dodawnia wpisu
   - w **Pole rozszerzone** wybierz TXT
   - w **Subdomena** wpisz _goxy
   - w **Wartość** wpisz ciąg znaków, który wyświetlił się w panelu Goxy podczas weryfikowania domeny

   ![zrzut ekranu pokazujący wyżej wspomniane kroki 1/3](images/domain-setup/ovh-2.png)
   ![zrzut ekranu pokazujący wyżej wspomniane kroki 2/3](images/domain-setup/ovh-3.png)
   ![zrzut ekranu pokazujący wyżej wspomniane kroki 3/3](images/domain-setup/ovh-4.png)

   Jeżeli wprowadziłeś potrzebne dane, możesz kliknąć przycisk **Dalej** oraz **Zatwiedź**, a następnie odczekać do 24 godzin, aby serwery nazw się odświeżyły. Po tym czasie wróć do panelu Goxy w celu ukończenia weryfikacji.

### Rekord SRV (kierowanie domeny na serwer)
1. Przejdź do swojej domeny w panelu OVH
2. Przejdź do zakładki **Strefa DNS** znajdującej się w menu nad informacjami o domenie
3. Kliknij przycisk **Dodaj rekord** znajdujący się po prawej stronie

   ![zrzut ekranu pokazujący miejsce, w którym znajduje się wyżej wspomniany przycisk](images/domain-setup/ovh-1.png)
4. Na stronie wyświetli się formularz dodawnia wpisu
   - w **Pole rozszerzone** wybierz SRV
   - w **Subdomena** wpisz _minecraft._tcp lub _minecraft._tcp.subdomena, jeżeli ją chcesz
   - w **Priorytet** wpisz 1
   - w **Waga** wpisz 1
   - w **Port** wpisz 25565
   - w **Adres docelowy** wpisz subdomenę swojej sieci z końcówką gxy.pl - znajdziesz ją w kategorii **Zweryfikowane domeny** w ustawieniach w panelu Goxy

   ![zrzut ekranu pokazujący wyżej wspomniane kroki 1/3](images/domain-setup/ovh-2.png)
   ![zrzut ekranu pokazujący wyżej wspomniane kroki 2/3](images/domain-setup/ovh-5.png)
   ![zrzut ekranu pokazujący wyżej wspomniane kroki 3/3](images/domain-setup/ovh-6.png)

   Jeżeli wprowadziłeś potrzebne dane, możesz kliknąć przycisk **Dalej** oraz **Zatwierdź**, a następnie odczekać do 24 godzin, aby serwery nazw się odświeżyły. Po tym czasie możesz spróbować wejść na serwer w grze przez swoją domenę.


---

## Mojego dostawcy domeny nie ma w tym poradniku

Jeżeli nie możesz znaleźć swojego dostawcy możesz wzorować się analogicznie innymi częściami tego poradnika. Konfiguracja rekordów w większości paneli przebiega w podobny sposób.

Niektórzy dostawcy nie oferują zarządzania rekordami zakupionych u nich domen. W takim przypadku nie jesteśmy w stanie nic poradzić, ponieważ dodanie rekordów jest kluczowe i nie da się tego ominąć. Należy się wtedy kontaktować z supportem dostawcy.
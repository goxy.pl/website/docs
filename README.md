# O Goxy

Goxy jest to zarządzana przez nas usługa proxy. Proxy znajduje się na naszych serwerach, a jego konfiguracja jest bardzo uproszczona i możliwa z poziomu specjalnie przeznaczonego do tego celu panelu.
Oprócz podstawowych funkcjonalności jakie można spotkać w innych proxy (Bungeecord, Velocity). Nasza usługa zawiera wbudowane logowanie graczy, automatyczne logowanie graczy premium, anty bota, podstawowy anty crasher oraz anty DDoS.
